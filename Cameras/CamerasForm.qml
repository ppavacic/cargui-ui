import QtQuick 2.4
import QtQuick.Layouts 1.13
import QtMultimedia 5.10
import org.ppavacic.statemachine 1.0


Item {
    id: cameraForm
    Layout.fillHeight: true
    Layout.fillWidth: true

    Camera {
        id: camera
        cameraState: stateMachine.displayState === StateMachine.Camera ?
                         Camera.ActiveState : Camera.UnloadedState
    }

    VideoOutput {
        source: camera
        anchors.fill: parent
    }
}
