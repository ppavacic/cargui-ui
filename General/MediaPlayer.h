#pragma once

#include <QObject>
#include <memory>
#include <QMediaPlayer>


class MediaPlayer : public QObject
{
    Q_OBJECT
public:
    MediaPlayer();
    Q_INVOKABLE void playAudio(QString source = "", QString audioName = "");
    Q_INVOKABLE void stopAudio();

    Q_PROPERTY(bool audioPaused READ audioPaused WRITE setAudioPaused NOTIFY audioPausedChanged)
    Q_PROPERTY(float audioVolume READ audioVolume WRITE setAudioVolume NOTIFY audioVolumeChanged)
    Q_PROPERTY(const QString& audioName READ audioName WRITE setAudioName NOTIFY audioNameChanged)
    Q_PROPERTY(QMediaPlayer* audioPlayer READ audioPlayer NOTIFY audioPlayerChanged)

    bool audioPaused() const { return m_audioPaused; }
    void setAudioPaused(bool audioPaused);
    float audioVolume() const { return m_audioVolume; };
    const QString& audioName() const { return m_audioName; };

    Q_INVOKABLE QMediaPlayer* audioPlayer() const { return m_audioPlayer.get(); };

public slots:
    void setAudioVolume(float percentage);
    void changeAudioSource(QString source);
    void setAudioName(const QString& audioName);
signals:
    void audioPausedChanged(bool audioPaused);
    void audioVolumeChanged(float audioVolume);
    void audioNameChanged(const QString& newAudioName);
    void audioPlayerChanged();

    void prevAudioEntry();
    void nextAudioEntry();

private:
    float m_audioVolume = 0.5;
    bool m_audioPaused = true;
    std::unique_ptr<QMediaPlayer>
        m_audioPlayer, videoPlayer;
    QString m_audioName;
};

