#include "StateMachine.h"
#include <QQmlEngine>

StateMachine::StateMachine()
{
}


void StateMachine::setDisplayState(const int state) {
    m_displayState = state;
    emit displayStateChanged(state);
};
