import QtQuick 2.0
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.12

Item {
    z: 10
    Column {
        anchors.fill: parent

        ProgressBar {
            width:  window.width
            id: progressBar
            value: mediaPlayer.audioVolume
            visible: true
        }

        Text {
            id: audioSlider
            width: parent.width
            text: "Slide here to adjust volume"
            color: Material.foreground
            font.pixelSize: 15
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    MouseArea {
        anchors.fill: parent
        onPositionChanged: (mouse) => mediaPlayer.setAudioVolume(mouse.x/parent.width)//containsPress
    }

}
