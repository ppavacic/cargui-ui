#include "General/App.h"
#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    App app;
    return app.run(argc, argv);
}
