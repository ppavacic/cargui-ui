#pragma once
#include <QString>
#include <QStringList>
#include <QDebug>

class AppException {

public:
    enum SEVERITY {Info, Warning, Error, Critical};
    AppException(QString message, QString className = "Unknown class",
                 char severity = 'w') :
        message(message),
        severity(severity) {
        qInfo() << this->severity << ": "
                  << className
                  << ": "
                  << message;
    }
private:
    QString message;
    char severity;
};
