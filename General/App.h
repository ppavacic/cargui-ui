#pragma once
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "Music/MediaList.h"
#include "General/StateMachine.h"
#include "General/MediaPlayer.h"
#include "Music/Music.h"

class App
{
public:
    App();
    int run(int argc, char* argv[]);
private:
    std::unique_ptr<StateMachine> stateMachine;
    std::unique_ptr<MediaPlayer> mediaPlayer;
};
