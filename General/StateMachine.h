#pragma once
#include <QObject>
#include <memory>
#include <iostream>
#include <QDebug>
#include "States/MusicState.h"

class StateMachine : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int audioState READ audioState WRITE setAudioState NOTIFY audioStateChanged)
    Q_PROPERTY(int displayState READ displayState WRITE setDisplayState NOTIFY displayStateChanged)
    Q_PROPERTY(MusicState* musicState READ musicState NOTIFY musicStateChanged)
public:
    StateMachine();

    enum STATES { Music, /*Video,*/ Camera, Diagnostics, None };
    enum MUSIC_STATE_SOURCE { Radio, Local, USB };

    Q_ENUMS(STATES)
    Q_ENUMS(MUSIC_STATE_SOURCE)

    MusicState* musicState() { return &m_musicState; };


    int audioState() const { return m_audioState; };
    int displayState() const { return m_displayState; };

public slots:
    void setAudioState(const int state) { m_audioState = state; };
    void setDisplayState(const int state);

signals:
    void audioStateChanged(int state);
    void displayStateChanged(int state);
    void musicStateChanged();

private:
    int m_audioState = Radio;
    int m_displayState = Music;
    MusicState m_musicState;
};

