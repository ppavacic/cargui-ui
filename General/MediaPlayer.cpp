#include "MediaPlayer.h"
#include <iostream>
#include <QObject>

MediaPlayer::MediaPlayer()
{
    m_audioPlayer = std::make_unique<QMediaPlayer>(/*nullptr, QMediaPlayer::StreamPlayback*/);
    videoPlayer = std::make_unique<QMediaPlayer>();

    m_audioPlayer->setVolume(audioVolume()*100);
    videoPlayer->setVolume(audioVolume()*100);

    /*QObject::connect(m_audioPlayer.get(), &QMediaPlayer::mediaChanged, this, [&](QMediaContent con) {
        emit m_audioPlayer->durationChanged(42);
    });

    QObject::connect(m_audioPlayer.get(), &QMediaPlayer::durationChanged, this, [&](qint64 dur) {
        qDebug() << "duration = " << dur;
    });*/
}

void MediaPlayer::playAudio(QString source, QString audioName) {
    if(source == "" && m_audioPlayer->media().isNull()) {return;}
    else if(source != "" && source[0] != "/") m_audioPlayer->setMedia(QUrl(source));
    else if(source != "" && source[0] == "/") m_audioPlayer->setMedia(QUrl::fromLocalFile(source));
    if(audioName != "") setAudioName(audioName);
    setAudioPaused(false);
}

void MediaPlayer::stopAudio() {
    //TODO causes online stream to be "stored in buffer", check if that buffer can be enormous
    m_audioPlayer->pause();
    setAudioPaused(true);
}

void MediaPlayer::setAudioPaused(bool audioPaused) {
    m_audioPaused = audioPaused;
    if(audioPaused) m_audioPlayer->pause();
    else m_audioPlayer->play();

    emit audioPausedChanged(audioPaused);
}

void MediaPlayer::setAudioVolume(float percentage) {
    int newVolume = percentage * 100;
    if(newVolume < 0 || newVolume > 100) return;

    m_audioVolume = percentage;
    m_audioPlayer->setVolume(newVolume);
    emit audioVolumeChanged(m_audioVolume);
}

void MediaPlayer::changeAudioSource(QString source) {
    if(source[0] != "/") m_audioPlayer->setMedia(QUrl(source));
    else if(source[0] == "/") m_audioPlayer->setMedia(QUrl::fromLocalFile(source));
    if(!audioPaused()) m_audioPlayer->play();
}

void MediaPlayer::setAudioName(const QString& audioName) {
    m_audioName = audioName;
    emit audioNameChanged(m_audioName);
}

