import QtQuick 2.5
import QtQuick.Controls.Material 2.12

Item {
    id: clickableImage
    property var imageSrc
    property var onClicked
    state: "RELEASED"
    Image {
        id: image
        height: parent.height
        width: parent.height
        fillMode: Image.PreserveAspectFit
        source: clickableImage.imageSrc
        smooth: true
        z: 1
        anchors.fill: parent
    }
    Rectangle {
        anchors.centerIn: image
        id: fillRectangle
        height: image.height
        width: image.height
        radius: image.height/2
        color: Material.accent
        opacity: 0.0
        z: 0

        //ANIMATION STUFF
        states: [
            State {
                name: "PRESSED"
                PropertyChanges { target: fillRectangle; opacity: 1.0}
            },
            State {
                name: "RELEASED"
                PropertyChanges { target: fillRectangle; opacity: 0.0}
            }
        ]
        transitions: [
            Transition {
                from: "PRESSED"
                to: "RELEASED"
                OpacityAnimator { target: fillRectangle; duration: 250}
            },
            Transition {
                from: "RELEASED"
                to: "PRESSED"
                OpacityAnimator { target: fillRectangle; duration: 250}
            }
        ]
    }


    MouseArea {
        anchors.fill: image
        onClicked: parent.onClicked()
        onPressed: fillRectangle.state = "PRESSED"
        onReleased: fillRectangle.state = "RELEASED"
    }
}
