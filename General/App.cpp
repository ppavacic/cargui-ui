#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QQmlContext>

#include "General/App.h"


App::App()
{
    stateMachine = std::make_unique<StateMachine>();
    mediaPlayer = std::make_unique<MediaPlayer>();

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QQuickStyle::setStyle("Material");

    qmlRegisterType<StateMachine>("org.ppavacic.statemachine", 1, 0, "StateMachine");
    qmlRegisterType<MediaPlayer>("org.ppavacic.mediaplayer", 1, 0, "MediaPlayer");
}


int App::run(int argc, char* argv[])
{
    QGuiApplication app(argc, argv);
    app.setOrganizationName("ppavacic");
    app.setOrganizationDomain("ppavacic.org");

    const QUrl url("qrc:/General/main.qml");
    QQmlApplicationEngine engine;

    stateMachine->setAudioState(2);
    engine.rootContext()->setContextProperty(QStringLiteral("stateMachine"), stateMachine.get());
    engine.rootContext()->setContextProperty(QStringLiteral("mediaPlayer"), mediaPlayer.get());



    //after that MediaList should notify player that selected entry has changed


    auto connectRightMediaList = [&](){
        //delete connection to old medialist when source changes; when
        //stateMachine->musicState()->mediaSourceList() changes
        static QMetaObject::Connection changeAudioSourceConn, changeAudioNameConn;
        if(changeAudioNameConn && changeAudioNameConn) {
            QObject::disconnect(changeAudioSourceConn);
            QObject::disconnect(changeAudioNameConn);
        }

        changeAudioSourceConn = QObject::connect(
            stateMachine->musicState()->mediaSourceList(), &MediaList::mediaSourceChanged,
            mediaPlayer.get(), &MediaPlayer::changeAudioSource
        );

        changeAudioNameConn = QObject::connect(
            stateMachine->musicState()->mediaSourceList(), &MediaList::mediaNameChanged,
            mediaPlayer.get(), &MediaPlayer::setAudioName
        );
    };

    connectRightMediaList();


    QObject::connect(
        stateMachine->musicState(), &MusicState::musicSourceChanged,
        connectRightMediaList
    );

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
