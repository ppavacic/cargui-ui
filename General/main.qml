import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.12
import "../Diagnostics" as Diagnostics
import "../Cameras" as Cameras
import "../Video" as Video
import "../Music" as Music
import "../General" as General


import org.ppavacic.statemachine 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 1024
    height: 576
    objectName: "MainWindow"
    title: "Infotainment Preview"

    Material.theme: Material.Dark
    Material.accent: Material.DeepPurple

    ColumnLayout {
        anchors.fill: parent
        General.AudioControls {
            height: 20
            Layout.fillWidth: true
        }

        StackLayout {
            Layout.fillWidth: true
            //Layout.fillHeight: true
            currentIndex: stateMachine.displayState
            Music.MusicForm {
                id: musicTab
            }
            /*Video.VideoForm {
                id: videoTab
            }*/
            Cameras.CamerasForm {
                id: camerasTab
            }
            Diagnostics.DiagnosticsForm {
                id: diagnosticsTab
            }
        }
        TabBar {
            id: bar
            Layout.fillWidth: true
            Layout.preferredHeight: 40
            Layout.alignment: Qt.AlignBottom

            Repeater {
                model: ["Music", /*"Video",*/ "Cameras", "Diagnostics"]
                TabButton {
                    height: 40
                    text: qsTr(modelData)
                    onClicked: stateMachine.setDisplayState(index)
                }
            }
        }
    }
}


