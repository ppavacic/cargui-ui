import QtQuick 2.0
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.12

Item {
    property var mediaList
    property var mediaListId
    id: element
    Layout.fillWidth: true
    Layout.fillHeight: true
    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter


    ListView {
        id: columnLayout
        anchors.fill: parent
        model: mediaList.mediaNames
        delegate: ItemDelegate {
            id: audioEntry
            font.pixelSize: 20
            text: qsTr(modelData)
            width: parent.width
            highlighted:
                index === mediaList.selectedMedia
                && stateMachine.musicState.musicSource === stateMachine.musicState.displaySource
            onClicked: {
                stateMachine.musicState.musicSource = stateMachine.musicState.displaySource
                mediaList.setSelectedMedia(index)
                mediaPlayer.playAudio(
                    mediaList.getMediaSource(mediaList.selectedMedia),
                    mediaList.getMediaName(mediaList.selectedMedia)
                )
            }
            background: Rectangle {
                anchors.fill: audioEntry
                color: audioEntry.highlighted ? "gray" : "transparent"
            }
        }
    }

}
