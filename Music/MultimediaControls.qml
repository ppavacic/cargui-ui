import QtQuick 2.0
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import "../General" as General
import QtQuick.Controls.Material 2.12

Item {
    Layout.alignment: Qt.AlignBottom | Qt.AlignHCenter
    Material.theme: Material.Dark
    Material.accent: Material.Purple

    GridLayout {
        anchors.horizontalCenter: window.horizontalCenter
        Layout.fillWidth: true
        height: parent.height
        width: parent.width
        anchors.fill: parent
        rows: 2
        columns: 3
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        Text {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.row: 0
            Layout.column: 0
            Layout.rowSpan: 1
            Layout.columnSpan: 3
            text: mediaPlayer.audioName ?
                      qsTr(mediaPlayer.audioName)
                    : qsTr("No song playing")
            color: Material.foreground
            font.pixelSize: 25
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        General.ClickableImage {

            Layout.row: 1
            Layout.column: 1
            Layout.rowSpan: 1
            Layout.columnSpan: 1
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width/3
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            onClicked: {
                mediaPlayer.audioPaused ?
                    mediaPlayer.playAudio : mediaPlayer.stopAudio
            }
            imageSrc: {
                source: mediaPlayer.audioPaused ?
                        "qrc:/Images/Images/MediaControls/022-play.svg"
                        : "qrc:/Images/Images/MediaControls/020-pause.svg"
            }

        }

        General.ClickableImage {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Layout.row: 1
            Layout.column: 0
            Layout.rowSpan: 1
            Layout.columnSpan: 1
            Layout.preferredWidth: parent.width/3
            Layout.fillHeight: true
            onClicked: stateMachine.musicState.mediaSourceList.prevEntry
            imageSrc: "qrc:/Images/Images/MediaControls/014-left arrow.svg"
        }

        General.ClickableImage {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Layout.row: 1
            Layout.column: 2
            Layout.rowSpan: 1
            Layout.columnSpan: 1
            Layout.preferredWidth: parent.width/3
            Layout.fillHeight: true
            onClicked: stateMachine.musicState.mediaSourceList.nextEntry
            imageSrc: "qrc:/Images/Images/MediaControls/031-right arrow.svg"
        }
    }
}
