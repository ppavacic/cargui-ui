#pragma once
#include <QObject>
#include <QString>
#include <QDebug>
#include <QMediaPlayer>
#include <iostream>
#include <memory>


class MediaList : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList mediaNames READ mediaNames WRITE setMediaNames NOTIFY mediaNamesChanged)
    Q_PROPERTY(int selectedMedia READ selectedMedia WRITE setSelectedMedia NOTIFY selectedMediaChanged)
public:
    MediaList(QStringList mediaNames, QStringList mediaSources);
    MediaList(QString mediaPath);
    MediaList() = delete;

    Q_INVOKABLE QStringList mediaNames();

    Q_INVOKABLE void setMediaNames(QStringList newMediaNames);

    int selectedMedia();
    Q_INVOKABLE void setSelectedMedia(int selectedMedia);
    Q_INVOKABLE const QString getMediaSource(int index) const;
    Q_INVOKABLE QString getMediaName(int index);


    static QString setMediaPathPrefix(QString mediaPath);
signals:
    void mediaNamesChanged(QStringList);
    void mediaSourcesChanged(QStringList);
    void selectedMediaChanged(int selectedMedia);
    void mediaSourceChanged(QString newSource);
    void mediaNameChanged(QString newName);


public slots:
    void nextEntry();
    void prevEntry();

private:
    int m_selectedMedia = -1;
    const QString mediaPathPrefix;
    QStringList m_mediaNames;
    QStringList m_mediaSources;
};

