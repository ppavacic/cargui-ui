#include <QDir>
#include "MediaList.h"

MediaList::MediaList(QStringList mediaNames, QStringList mediaSources) :
    mediaPathPrefix(""),
    m_mediaNames(mediaNames),
    m_mediaSources(mediaSources)
{
    if(mediaNames.length() != mediaNames.length())
        qFatal("!Provided different number of mediaNames names and mediaSources");
}

MediaList::MediaList(QString mediaPath) :
    mediaPathPrefix(MediaList::setMediaPathPrefix(mediaPath)) {
    QDir directory(mediaPathPrefix);
    setMediaNames(directory.entryList(QStringList() << "*.mp3" << "*.wav" << "*.ogg"));
}


QStringList MediaList::mediaNames() {
    return m_mediaNames;
}


int MediaList::selectedMedia() {
    return m_selectedMedia;
}

void MediaList::setSelectedMedia(int selectedMedia) {
    m_selectedMedia = selectedMedia;
    emit selectedMediaChanged(selectedMedia);
    emit mediaSourceChanged(getMediaSource(selectedMedia));
    emit mediaNameChanged(getMediaName(selectedMedia));
}

void MediaList::setMediaNames(QStringList newMediaNames) {
    m_mediaNames = newMediaNames;
    emit mediaNamesChanged(newMediaNames);
}

const QString MediaList::getMediaSource(int index) const {
    if(mediaPathPrefix == "") return m_mediaSources[index];
    return mediaPathPrefix + m_mediaNames[index];
}

QString MediaList::getMediaName(int index) {
    return m_mediaNames[index].section(".", 0, 0);
}

void MediaList::nextEntry() {
    int newSelectedMedia = (selectedMedia() + 1)% mediaNames().length();
    setSelectedMedia(newSelectedMedia);
}

void MediaList::prevEntry() {
    int newSelectedMedia =
       (selectedMedia() - 1 >= 0) ?
       (selectedMedia() - 1) : mediaNames().length() -1;
    setSelectedMedia(newSelectedMedia);
}

QString MediaList::setMediaPathPrefix(QString mediaPath) {
    if(mediaPath[mediaPath.length()-1] == "/") return mediaPath;
    return mediaPath + "/";
}
