import QtQuick 2.4
import QtQuick.Layouts 1.13
import QtQuick.Controls 2.5
import QtQuick.Scene3D 2.0

import "../General" as General

Item {
    id: musicForm
    Layout.fillHeight: true
    Layout.fillWidth: true
    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

    GridLayout {
        id: musicGrid
        rowSpacing: 5
        anchors.fill: parent
        Layout.fillHeight: true
        Layout.fillWidth: true

        columns: 4
        rows: 4

        MediaList {
            id: mediaList
            Layout.column: 0
            Layout.row: 0
            Layout.columnSpan: 2
            Layout.rowSpan: 2

            Layout.fillHeight: true
            Layout.preferredWidth: 2*parent.width/5
            mediaList: stateMachine.musicState.mediaDisplayList
        }
        /*Item {
            Layout.column: 3
            Layout.columnSpan: 1
            Layout.row: 0
            Layout.rowSpan: 2
            Layout.preferredWidth: 2*parent.width/5
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

            Image {
                //anchors.centerIn: window
                width: parent.width * 0.35
                height: parent.width * 0.35
                anchors.fill: parent
                source: "qrc:/Images/Images/Placeholders/song.png"
                smooth: true
            }
            ListView {
                anchors.fill: parent
                model: Text {}
                delegate: contactDelegate
                highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
                focus: true
            }
        }*/



        MultimediaControls {
            Layout.column: 0
            Layout.row: 2
            Layout.columnSpan: 4
            Layout.rowSpan: 1
            Layout.fillWidth: true
            Layout.preferredHeight: parent.height/6
        }

        Item {
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width/8
            Layout.alignment: Qt.AlignRight
            Layout.column: 3
            Layout.row: 0
            Layout.columnSpan: 1
            Layout.rowSpan: 2

            ColumnLayout {
                spacing: 5
                anchors.fill: parent
                Repeater {
                    model: [
                        "013-info.svg", "004-folder.svg", //radio and local storage
                        "034-usb.svg", //"002-bluetooth.svg" //usb and bluetooth
                    ]
                    General.ClickableImage {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                        Layout.preferredWidth: parent.width -50
                        Layout.preferredHeight: parent.width -50
                        imageSrc: "qrc:/Images/Images/AudioSources/" + modelData
                        onClicked: () => stateMachine.musicState.setDisplaySource(index)
                    }
                }
            }
        }

        Item {
            Layout.row: 3
            Layout.column: 0
            Layout.columnSpan: 4
            Layout.rowSpan: 1
            Layout.preferredWidth: Layout.maximumWidth

            ProgressBar {
                width: parent.width
                id: progressBar
                value: {//console.log(mediaPlayer.audioPlayer.duration);
                    return mediaPlayer.audioPlayer.duration/ 100}
            }
        }


    }

}
