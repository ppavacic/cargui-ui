#include <QObject>
#include <memory>
#include "Music/MediaList.h"
#include "General/ExceptionHandler.h"

class MusicState : public QObject  {
    Q_OBJECT
    Q_PROPERTY(int musicSource READ musicSource WRITE setMusicSource NOTIFY musicSourceChanged)
    Q_PROPERTY(int displaySource READ displaySource WRITE setDisplaySource NOTIFY displaySourceChanged)

    Q_PROPERTY(MediaList* mediaSourceList READ mediaSourceList NOTIFY mediaSourceListChanged)
    Q_PROPERTY(MediaList* mediaDisplayList READ mediaDisplayList NOTIFY mediaDisplayListChanged)

public:
    //MusicState();
    MusicState();

    Q_INVOKABLE MediaList* mediaSourceList() {
        switch(musicSource()) {
            case Radio:
                return &m_radioList;
            case Local:
                return &m_localList;
            case USB:
                return &m_usbList;
            default:
                throw AppException("Unhandled media source!", "MusicState");
        }
    };

    Q_INVOKABLE MediaList* mediaDisplayList() {
        switch(displaySource()) {
            case Radio:
                return &m_radioList;
            case Local:
                return &m_localList;
            case USB:
                return &m_usbList;
            default:
                throw AppException("Unhandled media source!", "MusicState");
        }
    };

    enum SOURCES { Radio, Local, USB, Bluetooth };
    Q_ENUMS(SOURCES);

    MusicState(const MusicState&) = delete;
    void operator=(MusicState const&) = delete;

    int musicSource() const { return m_musicSource; };
    int displaySource() const { return m_displaySource; };

    Q_INVOKABLE void setDisplaySource(int newSource) {
        if(m_displaySource == newSource) return;
        m_displaySource = newSource;
        emit displaySourceChanged();
        emit mediaDisplayListChanged();
    };


    Q_INVOKABLE void setMusicSource(int newSource) {
        if(m_musicSource == newSource) return;
        m_musicSource = newSource;
        emit musicSourceChanged();
        emit mediaSourceListChanged();
    };

signals:
    void mediaSourceListChanged();
    void mediaDisplayListChanged();

    void musicSourceChanged();
    void displaySourceChanged();
private:
    int m_musicSource = Radio;  // which music list is used in interaction (next, last song button)
    int m_displaySource = Radio; // which music list are we displaying

    MediaList m_radioList;
    MediaList m_localList;
    MediaList m_usbList;
};
