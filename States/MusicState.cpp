#include "States/MusicState.h"
#include "General/Common.h"
MusicState::MusicState() :
    m_radioList(
        QStringList{
            "Narodni", "Laganini", "Rijeka", "Otvoreni",
            "Antena Dance", "Antena Zagreb", "Banovina", "Extra FM",
            "Impulse Techno", "Impulse Rock", "Impulse Oldies", "Impulse Jazz"

        },
        QStringList{
            "http://live.narodni.hr:8059/stream/1/",
            "https://onlineradiobox.com/json/hr/soundsetplavi/play?platform=web",
            "http://21223.live.streamtheworld.com:3690/RIJEKAAAC_SC",
            "https://otrlive.radioca.st/streams/64kbps",

            "http://live.antenazagreb.hr:8015/",
            "http://live.antenazagreb.hr:8002/;",
            "https://5b57bb229a2e6.streamlock.net/live/televizija/media_w87033309_15826.ts",
            "http://streams.extrafm.hr:8110/;",

            "http://orion.shoutca.st:8938/stream",
            "http://5.39.71.159:8871/stream",
            "http://5.39.71.159:8248/stream",
            "http://orion.shoutca.st:8950/stream"
        }
    ),
    m_localList(LOCAL_FOLDER),
    m_usbList(USB_FOLDER)
{}
