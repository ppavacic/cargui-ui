import QtQuick 2.4
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls.Material 2.12

Item {


    Item {
        width: 400
        height: 400
        anchors.centerIn: parent
    GridLayout {
        anchors.fill: parent
        columns: 2

        Repeater {
            model: [
                "Engine", "Last oil check", "Tire pressure", "Brake tear",
                "Wiper fluid", "Engine temperature", "Highest engine temperature"
            ]
            Text {
                Layout.column: 0
                Layout.row: index
                Layout.columnSpan: 1
                Layout.rowSpan: 1
                text: modelData + ": "
                color: Material.foreground
                font.pixelSize: 15
            }
        }

        Repeater {
            model: [
                "Ok", "2020-15-5", "2.8 BAR", "1%",
                "65%", "65 C", "95 C"

            ]
            Text {
                Layout.column: 1
                Layout.row: index
                Layout.columnSpan: 1
                Layout.rowSpan: 1
                text: modelData
                color: Material.foreground
                font.pixelSize: 15
            }
        }
    }
    }
}
